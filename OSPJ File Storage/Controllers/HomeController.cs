﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSPJ_File_Storage.Controllers
{
    public class HomeController : Controller
    {
        //Controller to be used mainly for navigations in homepage

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult DefaultHomePage()
        {

            return View();
        }

        public ActionResult MyFilesH()
        {
            return View();
        }

        public ActionResult DefaultHomePageH()
        {
            return View();
        }
    }
}