﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OSPJ_File_Storage.Controllers
{
    public class ESignatureController : Controller
    {
        //Set this controller for all methods related to esigning the files.


        // GET: ESignature/Index
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddSignatory()
        {
            return View();
        }

        public ActionResult ESign()
        {
            return View();
        }
        public ActionResult RequestSignature()
        {
            return View();
        }

        public ActionResult SignatureStatus()
        {
            return View();
        }

        public ActionResult DefaultHomePage()
        {
            return View();
        }
    }
}