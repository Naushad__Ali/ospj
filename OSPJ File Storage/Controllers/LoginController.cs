﻿using OSPJ_File_Storage.Models;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AspNetCore.Totp;

namespace OSPJ_File_Storage.Controllers
{
    public class LoginController : Controller
    {
        //This controller is designed for creating&destroying login sessions.

        // GET: Login
        
        public ActionResult LoginPage()
        {
            return View();
        }

        public ActionResult OTPVerify()
        {
            return View();
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session.Remove("User");
            return View("~/views/home/index.cshtml");
        }

        
        [Route("LoginSubmit")]
        [HttpPost]
        public ActionResult LoginSubmit(LoginFormModel loginFormModel)
        {
            if (loginFormModel.Username == null)
            {
                ViewData["ErorMessage"] = "Username is required";
                return View("~/views/Login/LoginPage.cshtml");
            }
            Database db = new Database();
            bool loginSuccessful = db.CheckLoginIsSuccessful(loginFormModel);
            bool userExist = db.CheckUserExist(loginFormModel);
            Debug.WriteLine(userExist);

            if (!loginSuccessful)
            {
                if (userExist == true)
                {
                    int failcount = db.CheckFailCount(loginFormModel);
                    if (failcount > 4)
                    {
                        //Direct to honey pot page
                        return View("~/Views/Home/DefaultHomePageH.cshtml");
                    }
                    else
                    {
                        failcount++;
                        Debug.WriteLine(failcount);
                        db.UpdateFailCount(loginFormModel, failcount);
                        ViewData["ErorMessage"] = "Login failed, invalid username/password";
                        return View("~/views/Login/LoginPage.cshtml");
                    }
                }
                else
                ViewData["ErorMessage"] = "Login failed, invalid username/password";
                return View("~/views/Login/LoginPage.cshtml");
                
                
            }

            // if login successful, set user session
            Session["User"] = loginFormModel.Username;
            db.UpdateFailCount(loginFormModel, 0);
            // and check if user has setup 2 FA

            bool userSetuped2FA = db.CheckIfUserSetUp2FA(loginFormModel);

            if (userSetuped2FA)
            {
                return View("~/views/Login/LoginWith2FA.cshtml");
            }
            else
            {
                var totpSetupGenerator = new TotpSetupGenerator();
                var totpSetup = totpSetupGenerator.Generate("FileShareApp", Session["User"].ToString(), "YourSuperSecretKeyHere" + Session["User"], 300, 300);

                // generate QRcode and put inside viewdata to display
                ViewData["QRImage"] = totpSetup.QrCodeImage;
                ViewData["ManualSetupKey"] = totpSetup.ManualSetupKey;

                return View("~/views/Login/Setup2fa.cshtml");
            }
        }

        [Route("Submit2FAComplete")]
        [HttpPost]
        public ActionResult Submit2FAComplete()
        {
            Database db = new Database();
            db.Completed2FASetup(Session["User"].ToString());

            
            return View("~/views/Home/index.cshtml");
        }

        [Route("Check2FAPin")]
        [HttpPost]
        public ActionResult Check2FAPin(Google2FAModel google2FModel)
        {
            var totpValidator = new TotpValidator(new TotpGenerator());
            bool isCorrectPIN = totpValidator.Validate("YourSuperSecretKeyHere" + Session["User"], Convert.ToInt32(google2FModel.Pin));

            if (isCorrectPIN  )
            {
                return View("~/views/Home/index.cshtml");

            }
            else
            {
                ViewData["ErorMessage"] = "Invalid Pin!";
                return View("~/views/Login/LoginWith2FA.cshtml");
            }
        }
    }
}