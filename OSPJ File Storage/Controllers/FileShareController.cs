﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using OSPJ_File_Storage.Models;
using System.Security.Cryptography;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Data;

namespace OSPJ_File_Storage.Controllers
{
    public class FileShareController : Controller
    {
        // GET: FileShare
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult StoredFiles()
        {
            return View();
        }

        public ActionResult UploadFile()
        {
            return View();
        }

        public ActionResult MyFiles()
        {
            return View();
        }

       
        public ActionResult UploadFileH()
        {
            return View();
        }

        public ActionResult StoredFilesH()
        {
            return View();
        }

        //UPLOAD AND DOWNLOAD 
        
        SqlConnection con;
        
       



       
        

        public int CheckId()
        {
            con = new SqlConnection(constrLog);
            string sql = "select count(*) from [logs]";
            SqlCommand c = new SqlCommand(sql, con);
            int temp = Convert.ToInt32(c.ExecuteScalar().ToString());
            temp++;
            con.Close();
            return temp;
        }
        //test
        public void DlLog()
        {
            con = new SqlConnection(constrLog);
            con.Open();
            SqlCommand sc = new SqlCommand("SELECT * FROM [Logs]", con);
            SqlDataAdapter da = new SqlDataAdapter(sc);
            DataSet ds = new DataSet();
            da.Fill(ds);

            ds.WriteXml("MyFile.txt");

            con.Close();
        }

        //shr
        // GET: UploadFile
        static string password = "ThePasswordToDecryptAndEncryptTheFile";

        static int id = 1;
        static String constr = @"Data Source=192.168.1.82; User ID=sa;Password=password; Initial Catalog=HoneyUpload";
        static String constrLog = @"Data Source=192.168.1.82; User ID=sa;Password=password;Initial Catalog=HoneyLogs";


        [HttpPost]
        public ActionResult Index(HttpPostedFileBase postedFile)
        {

            if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                postedFile.SaveAs(path + Path.GetFileName(postedFile.FileName));
                ViewBag.Message = "File uploaded successfully.";
                FileEncrypt(path + Path.GetFileName(postedFile.FileName));
                //
                using (SqlConnection con = new SqlConnection(constrLog))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "Insert into [Logs] values ('" + CheckId() + "','" + DateTime.Now.ToString("MM/dd/yyyy hh:mm tt") + "','upload','testfilename.txt'";
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteScalar();
                        //
                    }




                    return View(GetFiles());
                }
            }

            else
            {
                Debug.WriteLine("No File Chosen");
                return null;
            }
        }

        [HttpPost]
        public FileResult DownloadFile(int? fileId)
        {
            byte[] bytes;
            string fileName, contentType;
            using (SqlConnection con = new SqlConnection(constrLog))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Insert into [Logs] values ('" + CheckId() + "','" + DateTime.Now.ToString("MM/dd/yyyy hh:mm tt") + "','Download','testfilename.txt'";
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteScalar();

                }
            }
                using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT Name, Data, ContentType FROM fakefiles WHERE Id=@Id";
                    cmd.Parameters.AddWithValue("@Id", fileId);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        sdr.Read();
                        bytes = (byte[])sdr["Data"];
                        contentType = sdr["ContentType"].ToString();
                        fileName = sdr["Name"].ToString();
                    }

                    string path = Server.MapPath("~/Downloads/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                        //String decrypted = FileDecrypt(Path.GetFileName(fileName), path + Path.GetFileName(fileName) + "decrypted", password).Name;
                        return File(bytes, contentType, fileName);

                    }
                    //String decrypted1 = FileDecrypt(fileNamepath + Path.GetFileName(fileName) + "decrypted", password).Name;
                    //  con.Close();
                    return File(bytes, contentType, fileName);

                }
            }

        }
            

        private static List<FileModel> GetFiles()
        {
            List<FileModel> files = new List<FileModel>();
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Id, Name FROM fakefiles"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            files.Add(new FileModel
                            {
                                Id = Convert.ToInt32(sdr["Id"]),
                                Name = sdr["Name"].ToString()
                            });
                        }
                    }
                    con.Close();
                }
            }
            return files;
        }
        //  Call this function to remove the key from memory after use for security
        [DllImport("KERNEL32.DLL", EntryPoint = "RtlZeroMemory")]
        public static extern bool ZeroMemory(IntPtr Destination, int Length);

        /// <summary>
        /// Creates a random salt that will be used to encrypt your file. This method is required on FileEncrypt.
        /// </summary>
        /// <returns></returns>
        public static byte[] GenerateRandomSalt()
        {
            byte[] data = new byte[32];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                for (int i = 0; i < 10; i++)
                {
                    // Fille the buffer with the generated data
                    rng.GetBytes(data);
                }
            }

            return data;
        }

        /// <summary>
        /// Encrypts a file from its path and a plain password.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="password"></param>
        private void FileEncrypt(string inputFile)
        {
            //http://stackoverflow.com/questions/27645527/aes-encryption-on-large-files

            //generate random salt
            byte[] salt = GenerateRandomSalt();

            //create output file name
            FileStream fsCrypt = new FileStream(inputFile + ".aes", FileMode.Create);



            //convert password string to byte arrray
            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);

            //Set Rijndael symmetric encryption algorithm
            RijndaelManaged AES = new RijndaelManaged();
            AES.KeySize = 256;
            AES.BlockSize = 128;
            AES.Padding = PaddingMode.PKCS7;

            //http://stackoverflow.com/questions/2659214/why-do-i-need-to-use-the-rfc2898derivebytes-class-in-net-instead-of-directly
            //"What it does is repeatedly hash the user password along with the salt." High iteration counts.
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);

            //Cipher modes: http://security.stackexchange.com/questions/52665/which-is-the-best-cipher-mode-and-padding-mode-for-aes-encryption
            AES.Mode = CipherMode.CFB;

            // write salt to the begining of the output file, so in this case can be random every time
            fsCrypt.Write(salt, 0, salt.Length);

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateEncryptor(), CryptoStreamMode.Write);

            FileStream fsIn = new FileStream(inputFile, FileMode.Open);

            //create a buffer (1mb) so only this amount will allocate in the memory and not the whole file
            byte[] buffer = new byte[1048576];
            int read;

            try
            {
                while ((read = fsIn.Read(buffer, 0, buffer.Length)) > 0)
                {

                    cs.Write(buffer, 0, read);
                }

                // Close up
                fsIn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                cs.Close();
                fsCrypt.Close();
            }
            string filename = (inputFile + ".aes").ToString();
            byte[] bytes = System.IO.File.ReadAllBytes(filename);

            //  using (BinaryReader br = new BinaryReader(System.IO.File.Open(filename, FileMode.Open)))
            //  {
            //      bytes = br.ReadBytes(fsCrypt.Length);
            //  }
            using (SqlConnection con = new SqlConnection(constr))
            {
                string query = "INSERT INTO fakefiles VALUES (@id, @Name, @ContentType, @Data)";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@Name", Path.GetFileName(fsCrypt.Name));
                    cmd.Parameters.AddWithValue("@ContentType", MimeMapping.GetMimeMapping(fsCrypt.Name));
                    cmd.Parameters.AddWithValue("@Data", bytes);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                id = id + 1;

            }
            fsCrypt.Close();
            System.IO.File.Delete(inputFile);
            System.IO.File.Delete(fsCrypt.Name);
        }

        /// <summary>
        /// Decrypts an encrypted file with the FileEncrypt method through its path and the plain password.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        /// <param name="password"></param>

        private FileStream FileDecrypt(string inputFile, string outputFile, string password)
        {
            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
            byte[] salt = new byte[32];

            FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);
            fsCrypt.Read(salt, 0, salt.Length);

            RijndaelManaged AES = new RijndaelManaged();
            AES.KeySize = 256;
            AES.BlockSize = 128;
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);
            AES.Padding = PaddingMode.PKCS7;
            AES.Mode = CipherMode.CFB;

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateDecryptor(), CryptoStreamMode.Read);

            FileStream fsOut = new FileStream(outputFile, FileMode.Create);

            int read;
            byte[] buffer = new byte[1048576];

            try
            {
                while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
                {

                    fsOut.Write(buffer, 0, read);
                }
            }
            catch (CryptographicException ex_CryptographicException)
            {
                Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            try
            {
                cs.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error by closing CryptoStream: " + ex.Message);
            }
            finally
            {
                fsOut.Close();
                fsCrypt.Close();
            }
            return fsOut;
        }

    }
}