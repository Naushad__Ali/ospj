﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.IO;
using System.Windows.Forms;

namespace OSPJ_File_Storage.Controllers
{
    public class CheckFileController : Controller
    {
        // GET: CheckFile
        public ActionResult CheckFile()
        {
            return View();
        }

        [Route("FileUpload")]
        [HttpPost]
        public ActionResult CheckFile(HttpPostedFileBase fileInput)
        {
            string result = new StreamReader(fileInput.InputStream).ReadToEnd();


            // Ic Number
            var icNumberMatches = Regex.Matches(result, @"[a-zA-Z]{1}[0-9]{7}[a-zA-Z]{1}", RegexOptions.IgnoreCase);
            if (icNumberMatches.Count > 0)
            {
                ViewData["IC"] = "Ic Number Found : " + icNumberMatches.Cast<Match>().Select(m => m.Value).ToList().FirstOrDefault();
                string message = "IC NUMBER DECTECTED!";
                MessageBox.Show(message);
            }


            // Bank card
            var bankCardMatches = Regex.Matches(result, @"[0-9]{4} [0-9]{4} [0-9]{4} [0-9]{4}", RegexOptions.IgnoreCase);
            if (bankCardMatches.Count > 0)
            {
                ViewData["CardNumber"] = "Bank Number Found : " + bankCardMatches.Cast<Match>().Select(m => m.Value).ToList().FirstOrDefault();
                string message = "BANK NUMBER DECTECTED!";
                MessageBox.Show(message);

            }



            return View("~/views/CheckFile/CheckFile.cshtml");
        }

    }
}