﻿using OSPJ_File_Storage.Models;
using System.Web.Mvc;

namespace OSPJ_File_Storage.Controllers
{
    public class ManageAccountController : Controller
    {
        // GET: ManageAccount
        public ActionResult MAccount()
        {
            return View();
        }

        [Route("Register")]
        [HttpPost]
        public ActionResult Register(RegisterModel registerModel)
        {
            Database db = new Database();

            if (registerModel.Password != registerModel.CPassword)
            {
                ViewData["RegisterMessage"] = "Password does not match !";
                return View("~/views/manageaccount/MAccount.cshtml");
            }

            if (db.UserIsRegistered(registerModel))
            {
                ViewData["RegisterMessage"] = "Username already registered !";
                return View("~/views/manageaccount/MAccount.cshtml");
            }

            db.RegisterUser(registerModel);
            ViewData["RegisterMessage"] = "Registration Successful !";
            return View("~/views/manageaccount/MAccount.cshtml");
        }

        [Route("ResetTwoFA")]
        [HttpPost]
        public ActionResult ResetTwoFA(ResetTwoFAModel resetTwoFAModel)
        {
            Database db = new Database();

            if (!db.UsernameExist(resetTwoFAModel.Username))
            {
                ViewData["Reset2FAMessage"] = "Username does not exist !";
                return View("~/views/manageaccount/MAccount.cshtml");
            }

            db.Reset2FA(resetTwoFAModel);

            ViewData["Reset2FAMessage"] = "2FA reset successfully !";
            return View("~/views/manageaccount/MAccount.cshtml");
        }

        [Route("ResetPassword")]
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            Database db = new Database();

            if (!db.UsernameExist(resetPasswordModel.Username))
            {
                ViewData["ResetPasswordMessage"] = "Username does not exist !";
                return View("~/views/manageaccount/MAccount.cshtml");
            }

            db.ResetPassword(resetPasswordModel);

            ViewData["ResetPasswordMessage"] = "Password reset successfully !";
            return View("~/views/manageaccount/MAccount.cshtml");
        }
    }
}