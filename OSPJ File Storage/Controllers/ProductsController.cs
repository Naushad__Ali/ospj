﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using OSPJ_File_Storage.Models;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace OSPJ_File_Storage.Controllers
{
    public class ProductsController : Controller
    {
        string key = "youtubee";
        SqlConnection con;
        SqlDataAdapter adapter;
        SqlCommand cmd;
        static String connectionString = @"Data Source=LAPTOP-FPTJ1TTF\jarry;Initial Catalog=FileUploadSystem;Integrated Security=True;";
        // GET: Products
        public ActionResult Index()
        {
            Products products = GetProducts();
            ViewBag.Message = "";
            return View(products);
        }
        public ActionResult ShowFiles()
        {
            Products products = GetProducts();
            ViewBag.Message = "";
            return View(products);
        }
        public void DownloadFile(String file)
        {
            try
            {
                String filename = file;
                String filepath = Server.MapPath("~/UploadedFile/" + filename);

                Response.Clear();
                Response.ClearHeaders();
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
                Response.Flush();

                Response.TransmitFile(filepath);
                Response.End();
                HttpContext.ApplicationInstance.CompleteRequest();
                ViewBag.Message = "";
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message.ToString();
            }

        }
        [HttpPost]
        public ActionResult Index(Products obj)
        {
            try
            {
                
                string strDateTime = System.DateTime.Now.ToString("ddMMyyyyHHMMss");
                string finalPath = "\\UploadedFile\\" + strDateTime + obj.UploadFile.FileName;
               

                obj.UploadFile.SaveAs(Server.MapPath("~") + finalPath);
                obj.FilePath = strDateTime + obj.UploadFile.FileName;
                ViewBag.Message = SaveToDB(obj);
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message.ToString();
            }

            Products products = GetProducts();
            return View(products);
        }
        static void EncryptFile(string filePath, string key)
        {
            byte[] plainContent = System.IO.File.ReadAllBytes(filePath);
            using (var DES = new DESCryptoServiceProvider())
            {
                DES.IV = Encoding.UTF8.GetBytes(key);
                DES.Key = Encoding.UTF8.GetBytes(key);
                DES.Mode = CipherMode.CBC;
                DES.Padding = PaddingMode.PKCS7;


                using (var memStream = new MemoryStream())
                {
                    CryptoStream cryptoStream = new CryptoStream(memStream, DES.CreateEncryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(plainContent, 0, plainContent.Length);
                    cryptoStream.FlushFinalBlock();
                    System.IO.File.WriteAllBytes(filePath, memStream.ToArray());
                   
                }
            }
        }
        private static void DecryptFile(string filePath, string key)
        {
            byte[] encrypted = System.IO.File.ReadAllBytes(filePath);
            using (var DES = new DESCryptoServiceProvider())
            {
                DES.IV = Encoding.UTF8.GetBytes(key);
                DES.Key = Encoding.UTF8.GetBytes(key);
                DES.Mode = CipherMode.CBC;
                DES.Padding = PaddingMode.PKCS7;


                using (var memStream = new MemoryStream())
                {
                    CryptoStream cryptoStream = new CryptoStream(memStream, DES.CreateDecryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(encrypted, 0, encrypted.Length);
                    cryptoStream.FlushFinalBlock();
                    System.IO.File.WriteAllBytes(filePath, memStream.ToArray());
                    Console.WriteLine("Decrypted succesfully " + filePath);
                }
            }
        }
        public string SaveToDB(Products obj)
        {
            try
            {
                con = new SqlConnection(connectionString);
                cmd = new SqlCommand();
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_AddFiles";
                cmd.Parameters.AddWithValue("@FileN", obj.FileN);
                cmd.Parameters.AddWithValue("@FilePath", obj.FilePath);
                cmd.ExecuteNonQuery();

                cmd.Dispose();
                con.Dispose();
                con.Close();

                return "Saved Successfully";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public Products GetProducts()
        {
            Products products = new Products();
            try
            {
                con = new SqlConnection(connectionString);
                cmd = new SqlCommand("Select * from tblFiles", con);
                con.Open();
                adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                adapter.Dispose();
                cmd.Dispose();
                con.Close();

                products.lstProducts = new List<Products>();

                foreach (DataRow dr in dt.Rows)
                {
                    products.lstProducts.Add(new Products
                    {
                        FileN = dr["FileN"].ToString(),
                        FilePath = dr["FilePath"].ToString()
                    });
                }

            }
            catch (Exception ex)
            {
                adapter.Dispose();
                cmd.Dispose();
                con.Close();
            }

            if (products == null || products.lstProducts == null || products.lstProducts.Count == 0)
            {
                products = new Products();
                products.lstProducts = new List<Products>();
            }

            return products;
        }
    }
}


