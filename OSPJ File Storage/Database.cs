﻿using OSPJ_File_Storage.Models;
using System;
using System.Data.SqlClient;

namespace OSPJ_File_Storage
{   //Official Proj
    public class Database
    {
        private readonly SqlConnection conn;
        
        public Database()
        {
            conn = new SqlConnection("Data Source=192.168.1.82;User ID =sa; Password=password;Initial Catalog=UserAccounts;");
        }

        public bool CheckLoginIsSuccessful(LoginFormModel loginFormModel)
        {
            conn.Open();
            string sql = "select count(*) from [Users] where username ='" + loginFormModel.Username + "' AND password = '" + loginFormModel.Password + "'";
            SqlCommand cmd = new SqlCommand(sql, conn);
            int temp = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            conn.Close();

            if (temp == 1)
            {
                return true;
            }

            return false;
        }
//by jarryl
        public bool CheckUserExist(LoginFormModel loginFormModel)
        {
            conn.Open();
            string sql = "select count(*) from [Users] where username ='" + loginFormModel.Username + "'";
            SqlCommand cmd = new SqlCommand(sql, conn);
            int temp = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            conn.Close();

            if (temp == 1)
            {
                return true;
            }

            return false;
        }

        public int CheckFailCount(LoginFormModel loginFormModel)
        {
            conn.Open();
            string sql = "select [failcount] from [Users] where username ='" + loginFormModel.Username + "'";
            SqlCommand cmd = new SqlCommand(sql, conn);
            int temp = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            conn.Close();
            return temp;
        }

        public void UpdateFailCount(LoginFormModel loginFormModel,int count)
        {
            conn.Open();
            string sql = "update [Users] set [failcount] = " + count + " where username ='" + loginFormModel.Username + "'";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.ExecuteScalar();
            conn.Close();
        }

        public bool CheckIfUserSetUp2FA(LoginFormModel loginFormModel)
        {
            conn.Open();
            string sql = "select count(*) from [Users] where username ='" + loginFormModel.Username + "' AND setup2fa = 1";
            SqlCommand cmd = new SqlCommand(sql, conn);
            int temp = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            conn.Close();

            if (temp == 1)
            {
                return true;
            }

            return false;
        }

        public void Completed2FASetup(string username)
        {
            conn.Open();
            string sql = "Update [Users] set setup2fa = 1 where username ='" + username + "' ";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.ExecuteScalar();
            conn.Close();
        }

        public bool UserIsRegistered(RegisterModel registerModel)
        {
            conn.Open();
            string sql = "select count(*) from [Users] where username ='" + registerModel.Username + "'";
            SqlCommand cmd = new SqlCommand(sql, conn);
            int temp = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            conn.Close();

            if (temp == 1)
            {
                return true;
            }

            return false;
        }

        public void RegisterUser(RegisterModel registerModel)
        {
            Random rnd = new Random();

            conn.Open();
            string sql = "insert into [Users] (id, username,password,setup2fa,fname,lname) values ('" + rnd.Next(10000, 99999).ToString() + "','" + registerModel.Username + "','" + registerModel.Password + "',0,'" + registerModel.Fname + "','" + registerModel.Lname + "') ";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.ExecuteScalar();
            conn.Close();
        }

        public bool UsernameExist(string username)
        {
            conn.Open();
            string sql = "select count(*) from [Users] where username ='" + username + "'";
            SqlCommand cmd = new SqlCommand(sql, conn);
            int temp = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            conn.Close();

            if (temp == 1)
            {
                return true;
            }

            return false;
        }

        public void Reset2FA(ResetTwoFAModel resetTwoFAModel)
        {
            conn.Open();
            string sql = "Update [Users] set setup2fa = 0 where username ='" + resetTwoFAModel.Username + "' ";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.ExecuteScalar();
            conn.Close();
        }

        public void ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            conn.Open();
            string sql = "Update [Users ] set password = '" + resetPasswordModel.Password + "' where username ='" + resetPasswordModel.Username + "' ";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.ExecuteScalar();
            conn.Close();
        }
    }
}