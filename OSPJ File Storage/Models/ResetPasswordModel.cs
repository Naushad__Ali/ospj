﻿namespace OSPJ_File_Storage.Models
{
    public class ResetPasswordModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}