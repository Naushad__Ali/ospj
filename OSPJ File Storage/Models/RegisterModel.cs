﻿namespace OSPJ_File_Storage.Models
{
    public class RegisterModel
    {
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string CPassword { get; set; }
    }
}