﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WUApiLib;
using System.Management;
using Renci.SshNet;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Collections;
using System.IO;

namespace OSPJ_File_Storage.Models
{
    public class Asset
    {
        // private string hostname;
        public string IpAddress { get; set; }

        public string OperatingSystem { get; set; }

        public List<string> InstalledApplications { get; set; }

        public List<string> InstalledWindowsUpdates { get; set; }

        public string PendingWindowsUpdates { get; set; }
        public ISearchResult SearchUpdates()
        {
            UpdateSession uSession = new UpdateSession();
            uSession.CreateUpdateSearcher();
            IUpdateSearcher uSearch = uSession.CreateUpdateSearcher();
            ISearchResult results = uSearch.Search("IsInstalled=0 and Type = 'Software'");

            foreach (IUpdate update in results.Updates)
            {
                Console.WriteLine(update);
            }

            return results;
        }

        public List<string> SortInstalledProgram(string unsortedPrograms)
        {

            List<string> list = new List<string>();

            Debug.WriteLine(unsortedPrograms);
            unsortedPrograms.Trim();


            StringReader stringReader = new StringReader(unsortedPrograms);

            // DebuggingMethods
            bool reachedEnd = false;
            while (reachedEnd == false)
            {
                string lineOfInstalled = stringReader.ReadLine();
                if (lineOfInstalled != null)
                {
                    Debug.WriteLine(lineOfInstalled);
                    list.Add(lineOfInstalled);
                }
                else
                {
                    Debug.WriteLine("All Installed Programs listed.");
                    reachedEnd = true;
                }
            }

            for (int i = 0; i < list.Count; i++)
            {
                string untrimmed = list[i];
                string trimmed = list[i].Trim();

                Debug.WriteLine(untrimmed);
                Debug.WriteLine(trimmed);
                list[i] = list[i].Trim();
            }

            ArrayList bufferToRemove = new ArrayList();
            for (int i = 0; i < list.Count; i++)
            {
                if ((string)list[i] == string.Empty || (string)list[i] =="||||" || (string)list[i] == "$_.Replace('\"','')" || (string)list[i] == "------------------")
                {
                    bufferToRemove.Add(i);
                }
            }

            for (int i = bufferToRemove.Count - 1; i >= 0; i--)
            {
                int[] buffer2 = new int[bufferToRemove.Count];
                bufferToRemove.CopyTo(buffer2);
                int a = buffer2[i];
                list.RemoveAt(a);
            }

            return list;
        }

        public List<string> SortWindowsUpdates(string unsortedUpdates)
        {
            List<string> unsortedUpdatesArray = new List<string>();

            StringReader read = new StringReader(unsortedUpdates);

            bool reachedEnd = false;
            while (reachedEnd == false)
            {
                string lineOfInstalled = read.ReadLine();
                if (lineOfInstalled != null)
                {
                    Debug.WriteLine(lineOfInstalled);
                    unsortedUpdatesArray.Add(lineOfInstalled);
                }
                else
                {
                    Debug.WriteLine("All Installed Updates listed.");
                    reachedEnd = true;
                }
            }


            ArrayList bufferToRemove = new ArrayList();
            for (int i = 0; i < unsortedUpdatesArray.Count; i++)
            {
                string untrimmed = unsortedUpdatesArray[i];
                string trimmed = unsortedUpdatesArray[i].Trim();

                Debug.WriteLine(untrimmed);
                Debug.WriteLine(trimmed);
                unsortedUpdatesArray[i] = unsortedUpdatesArray[i].Trim();
            }

            ;

            for (int i = 0; i < unsortedUpdatesArray.Count; i++)
            {
                if ((string)unsortedUpdatesArray[i] == string.Empty)
                {
                    bufferToRemove.Add(i);
                }
            }

            for (int i = bufferToRemove.Count - 1; i >= 0; i--)
            {
                int[] buffer2 = new int[bufferToRemove.Count];
                bufferToRemove.CopyTo(buffer2);
                int a = buffer2[i];
                unsortedUpdatesArray.RemoveAt(a);
            }
            ;
            return unsortedUpdatesArray;
        }

        public string SanitiseUninstallString(string uninstallString)
        {
            uninstallString = uninstallString.Insert(0,"\"");
            int exePlace = uninstallString.IndexOf("exe");
            Debug.WriteLine(uninstallString.IndexOf("exe"));
            uninstallString = uninstallString.Insert(exePlace + 3, "\"");

            Debug.WriteLine(uninstallString);
            return uninstallString;
        }
        public Asset GetAssetConfig(string ipAddress)
        {
            Asset remoteDevice = new Asset();
            remoteDevice.IpAddress = ipAddress;

            // Establishing SSH Connection To Remote System.
            using (var client = new SshClient(ipAddress, "Ali", "P@ssw0rd"))
            {
                client.Connect();

                // Get Systems OS
                string getSystemOS = " systeminfo | findstr /B /C:\"OS Name\" /C:\"OS Version\" ";
                var getOSCommand = client.CreateCommand(getSystemOS);
                getOSCommand.Execute();
                string systemOS = getOSCommand.Result;
                remoteDevice.OperatingSystem = systemOS;

                // Get List of Installed Applications
                var getInstalledApps = client.CreateCommand("powershell.exe -file GetInstalledPrograms.ps1 && type InstalledPrograms.csv");
                getInstalledApps.Execute();
                string installedAppsString = getInstalledApps.Result;
                List <string> installedAppsArray = SortInstalledProgram(installedAppsString);
                remoteDevice.InstalledApplications = installedAppsArray;

                // Get List of Windows update (Installed & Pending)
                var getPendingUpdates = client.CreateCommand("Powershell.exe -file GetPendingUpdates.ps1 && type PendingUpdates.csv");
                getPendingUpdates.Execute();
                string pendingUpdates = getPendingUpdates.Result;
                remoteDevice.PendingWindowsUpdates = pendingUpdates;

                var getInstalledUpdates = client.CreateCommand("Powershell.exe -file GetInstalledUpdates.ps1 && type InstalledUpdates.csv");
                getInstalledUpdates.Execute();
                string installedUpdates = getInstalledUpdates.Result;
                List<string> installedUpdatesArray = SortWindowsUpdates(installedUpdates);
                remoteDevice.InstalledWindowsUpdates = installedUpdatesArray;

            }

            Debug.WriteLine(remoteDevice.IpAddress);
            Debug.WriteLine(remoteDevice.OperatingSystem);
            Debug.WriteLine(remoteDevice.InstalledApplications);
            Debug.WriteLine(remoteDevice.InstalledWindowsUpdates);
            Debug.WriteLine(remoteDevice.PendingWindowsUpdates);

            return remoteDevice;
        }

        public void UninstallProgram(string ipAddress, string uninstallCommand)
        {
            string sanitisedUninstallCommand  = SanitiseUninstallString(uninstallCommand);
            using (var client = new SshClient(ipAddress, "Ali", "P@ssw0rd"))
            {
                   client.Connect();
                var uninstall = client.CreateCommand(sanitisedUninstallCommand);
                uninstall.Execute();
            }
        }
    }
}